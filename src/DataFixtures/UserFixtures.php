<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\User;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('user');
 		$password = $this->encoder->encodePassword($user, 'azerty');
        $user->setPassword($password);
        $user->setRoles([User::ROLE_USER, User::ROLE_ADMIN]);
        $manager->persist($user);

        $manager->flush();
    }
}
